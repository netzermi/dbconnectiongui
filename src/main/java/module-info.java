module at.hakimst.dbconnectiongui {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;

    opens at.hakimst.dbconnectiongui to javafx.fxml;
    exports at.hakimst.dbconnectiongui;
    exports at.hakimst.dbconnectiongui.controller;
    opens at.hakimst.dbconnectiongui.controller to javafx.fxml;
}